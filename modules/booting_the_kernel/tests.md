# Тесты по теме «Загрузка ядра Linux»

Copyright&nbsp;©&nbsp;2023–2024 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

## Single choice

Какие файлы необходимы u-boot для загрузки ядра Linux?

---

*	system.map, os
*	**vmlinux, initrd и dtb**
*	vmlinux, init, init.tab, rc.initd
*	vmlinux, init, rc.initd

## Single choice

Какая структура данных описывает аппаратную конфигурацию arm процессора в Linux?

---

*	initrd
*	MBR
*	**device tree**
*	firmware

## Single choice

На какие устройства обычно выдает информацию отладочная консоль uboot в процессе загрузки?

---

*	**serial**
*	i2c
*	spi
*	Ethernet

## Single choice

Какой протокол поддерживает u-boot для загрузки ядра Linux по сети?

---

*	http
*	ftp
*	**tftp**
*	scp

## Single choice

Какая структура данных описывает аппаратную конфигурацию arm soc в u-boot?

---

*	initrd
*	**device tree**
*	firmware
*	plug&play механизм шины Advanced System Bus
*	plug&play механизм шины Avalon

## Single choice

Какой процесс первым запускается ядром Linux в современных дистрибутивах?

---

*	grub
*	lilo
*	**systemd**
*	systemctl

## Single choice

Какая утилита может служит для запуска и останова сервиса?

---

*	insmod
*	modprobe
*	**systemctl**
*	start
