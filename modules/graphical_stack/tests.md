# Тесты по теме «Обзор графического стека в Linux»

Copyright&nbsp;©&nbsp;2023–2024 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

## Single choice

В каком адресном пространстве работает X-server?

---

*	**в пользовательском**
*	в ядре Linux
*	в адресном пространстве GPU

## Single choice

Какой тип драйвера фраме буфера?

---

*	**символьное**
*	блочное
*	сетевое
*	pci
